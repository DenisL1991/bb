#include "C:\Denis\temp\CubeMX\BikeProject\BikeProject\FSM\fsm.h"

volatile uint8_t state[NUM_OF_FSM] = {0};
volatile uint8_t msg[MSG_END];
volatile uint32_t Timer[TIMER_END];
volatile uint8_t TimerState[TIMER_END];
volatile uint8_t Messages[MSG_END];

void FSM_Init(void)
{
	for (uint8_t i = 0; i < TIMER_END; i++)
	{
		TimerState[i] = TIMER_STOPPED;
	}
	for (uint8_t i = 0; i < MSG_END; i++)
	{
		Messages[i] = 0;
	}
}
void StartTimer(uint8_t TimerID)
{
	Timer[TimerID] = 0;
	TimerState[TimerID] = TIMER_RUNNING;
}
void StopTimer(uint8_t TimerID)
{
	Timer[TimerID] = 0;
	TimerState[TimerID] = TIMER_STOPPED;
}
uint32_t GetTimer(uint8_t TimerID)
{
	return Timer[TimerID];
}
void VirtualTimerIncrement(void)
{
	for (uint8_t i = 0; i < TIMER_END; i++)
	{
		if (TimerState[i] == TIMER_RUNNING)
			Timer[i]++;
	}
}

void SendMessage(uint8_t Msg)
{
	if (Messages[Msg] == MESSAGE_DISABLE)
		Messages[Msg] = MESSAGE_ENABLE;
}
uint8_t GetMessage(uint8_t Msg)
{
	if (Messages[Msg] == MESSAGE_READ)
	{
		if (Msg <= MSG_END)
			return 1;
	}
	return 0;
}
void ProcessMessages(void)
{
	for (uint8_t i = 0; i < MSG_END; i++)
	{
		if (Messages[i] == MESSAGE_READ)
			Messages[i] = MESSAGE_DISABLE;
		else if (Messages[i] == MESSAGE_ENABLE)
			Messages[i] = MESSAGE_READ;
	}
}
