#include "C:\Denis\temp\CubeMX\BikeProject\BikeProject\FSM\fsm.h"

#define TIME_DEBOUNCE 1
#define TIME_TRIPPED 5

void FSM_ReadSwitch(void)
{
	switch (state[RS_FSM])
	{
		case 0:
			if (HAL_GPIO_ReadPin(ReadSwitch_GPIO_Port, ReadSwitch_Pin) == GPIO_PIN_SET)
			{
				state[RS_FSM] = 1;
				StartTimer(TIMER_RS);
			}
			break;
		case 1:
			if (HAL_GPIO_ReadPin(ReadSwitch_GPIO_Port, ReadSwitch_Pin) == GPIO_PIN_RESET)
			{
				state[RS_FSM] = 0;
				StopTimer(TIMER_RS);
			}
			else if (GetTimer(TIMER_RS) >= TIME_DEBOUNCE)
			{
				state[RS_FSM] = 2;
				SendMessage(MSG_RS_TRIPPED);
			}
			break;
		case 2:
			if (GetTimer(TIMER_RS) >= TIME_TRIPPED)
			{
				state[RS_FSM] = 0;
				StopTimer(TIMER_RS);
			}
			break;
	}
}
