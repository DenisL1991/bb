#ifndef __FSM_H
#define __FSM_H

#include "stm32f1xx_hal.h"
#include <stdint.h>
enum //��� ������������� ����� ���������
{
	BC_FSM		,
	NUM_OF_FSM
};
enum //����������� �������
{
	TIMER_BC		,
	TIMER_END
};
enum //��������� ���������
{
	MSG_RS_TRIPPED	,
	MSG_END
};

enum {TIMER_STOPPED, TIMER_RUNNING, TIMER_PAUSED}; //��������� �������
enum {MESSAGE_DISABLE, MESSAGE_ENABLE, MESSAGE_READ}; //��������� ���������

//������� ��� ������ � ����������� ��������
void StartTimer(uint8_t);
uint32_t GetTimer(uint8_t);
void StopTimer(uint8_t);
void VirtualTimerIncrement(void);

//������� ��� ������ � �����������
void SendMessage(uint8_t);
uint8_t GetMessage(uint8_t);
void ProcessMessages(void);

//�������������
void FSM_Init(void);

//��������
void FSM_BikeComp(void);		//������� ��������������	

extern volatile uint8_t state[NUM_OF_FSM];

#endif
