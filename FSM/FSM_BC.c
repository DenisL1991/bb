#include "C:\Denis\temp\CubeMX\BikeProject\BikeProject\FSM\fsm.h"
#include "usart.h"

#define WHEEL_SIZE 2075			//������ ������ � ��
#define TIME_DEBOUNCE 10		//����������� ����� ����� �������������� �������
#define TIME_UPDATE 1000		//����� ���������� ������
#define TIME_STOP 5000			//������������ ����� ����� �������������� �������
#define CONVERT_TIME 3600		//������� �� �� � ����
#define DIG1_OF_TIME 60			//������� ��� ���������� ����� � ������
#define DIG2_OF_TIME 1000		//������� �� ���������� � ����
#define CONVERT_DIST 10000	//������� �� ���������� � ���������
#define DIG_OF_DIST 100			//������� ��� ���������� ����������

volatile uint16_t	real_time = 0;	//������ �� ������� ���������� ������
volatile uint8_t real_turn = 0;		//���������� ��������
volatile uint32_t total_time = 0;	//����� ����� �� �������
volatile uint32_t total_turn = 0;	//����� ���������� ��������
volatile uint8_t min_time = 0;		//����������� ����� ����� ��������������

uint8_t *fract(uint16_t num)
{
	uint8_t buf[2] = {0};
	if (num < 10) {buf[0] = 0; buf[1] = num;}
	else {buf[0] = num/10; buf[1] = num%10;}
	uint8_t *b = buf;
	return b;
}
void MaxSpeed(uint32_t time)
{
	if (time)
		min_time = (time < min_time) ? time : min_time;
}
uint8_t *SpeedCalc(uint32_t time, uint32_t turn)
{
	uint8_t buf[6] = {0};
	uint32_t  sp  = DIG2_OF_TIME*(turn*WHEEL_SIZE/CONVERT_DIST)/(time/CONVERT_TIME);
	uint8_t   sp1 = sp/DIG_OF_DIST;
	uint8_t  *sp2 = fract(sp - sp1*DIG_OF_DIST);
	sprintf(buf, "%d.%d%d", sp1, sp2[0], sp2[1]);
	uint8_t *b = buf;
	return b;
}
uint8_t *TimeCalc(uint32_t time)
{
	uint8_t buf[9] = {0};
	time = time/CONVERT_TIME;
	uint8_t hour = time/DIG2_OF_TIME;
	uint8_t minutes_temp = ((time - hour*DIG2_OF_TIME)*DIG1_OF_TIME)/DIG2_OF_TIME;
	uint8_t* minutes[2] = {&fract(minutes_temp)[0], &fract(minutes_temp)[1]};
	uint8_t seconds_temp = (((time - hour*DIG2_OF_TIME)*DIG1_OF_TIME) - minutes_temp*DIG2_OF_TIME)
	*DIG1_OF_TIME/DIG2_OF_TIME;
	uint8_t* seconds[2] = {&fract(seconds_temp)[0], &fract(seconds_temp)[1]};
	sprintf(buf, "%d:%d%d:%d%d", hour, minutes[0], minutes[1], seconds[0], seconds[1]);
	uint8_t *b = buf;
	return b;
}
uint8_t *DistCalc(uint32_t turn)
{
	uint8_t buf[6] = {0};
	uint32_t to = (turn*WHEEL_SIZE)/CONVERT_DIST;
	uint8_t to1 = to/DIG_OF_DIST;
	uint8_t * to2 = fract(to - to1 * DIG_OF_DIST);
	sprintf(buf, "%d.%d%d", to1, to2[0],to2[1]);
	uint8_t *b = buf;
	return b;
}
void DisplayData(void)
{
	uint8_t buffer[128] = {0};
	//sprintf(buffer, "\r\nSpeed: %i.%i\r\nTotal odo: %i.%i\r\nTotal time: %i:%i:%i\r\n	Aver Speed: %i.%i\r\nMax Speed: %i.%i\r\n_______________\r\n", 
	//RealSpeed1, RealSpeed2, TotalDistance1, TotalDistance2, TotalTime1, TotalTime2, TotalTime3, 
	//AverageSpeed1, AverageSpeed2, MaxSpeed1, MaxSpeed2);

	HAL_UART_Transmit_IT(&huart2, buffer,120);
}

void FSM_BikeComp(void)
{
	switch (state[BC_FSM])
	{
		case 0:
			if (GetMessage(MSG_RS_TRIPPED))
			{
				StartTimer(TIMER_BC);
				state[BC_FSM] = 1;
			}
			break;
		case 1:
			if (GetMessage(MSG_RS_TRIPPED) && GetTimer(TIMER_BC) > TIME_DEBOUNCE)
			{
				if (GetTimer(TIMER_BC) > TIME_STOP)
				{
					StopTimer(TIMER_BC);
					real_time = 0;
					real_turn = 0;
					state[BC_FSM] = 0;
				}
				else if (real_time < TIME_UPDATE)
				{
					real_time = real_time + GetTimer(TIMER_BC);
					real_turn++;
					MaxSpeed(GetTimer(TIMER_BC));
					StopTimer(TIMER_BC);
					StartTimer(TIMER_BC);
				}
				else
				{
					real_time = real_time + GetTimer(TIMER_BC);
					real_turn++;
					total_time = total_time + real_time;
					total_turn = total_turn + real_turn;
					MaxSpeed(GetTimer(TIMER_BC));	
					DisplayData();
					real_time = 0;
					real_turn = 0;
					StopTimer(TIMER_BC);
					StartTimer(TIMER_BC);
				}
			}
			break;
	}
}
